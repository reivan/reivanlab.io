---
layout: post
title:  "CTRL+Ы or how to use key bindings with russian layout in Sublime Text"
date:   2016-10-27
tags: development
---

Key bindings like `ctrl+c`, `ctrl+z` and `ctrl+s` didn't work in my Sublime Text when I switched to russian layout. This happened on Linux Mint with Cinnamon desktop environment but it probably appears in Ubuntu also.

I solved the problem by adding new key bindings to `Preference > Key Bindings`:

    { "keys": ["ctrl+с"], "command": "copy" },
    { "keys": ["ctrl+я"], "command": "undo" },
    { "keys": ["ctrl+shift+я"], "command": "redo" },
    { "keys": ["ctrl+ы"], "command": "save" },

I still can't believe it worked but `ctrl+ы` sounds awesome!
