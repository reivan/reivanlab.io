---
layout: post
title:  "Showing Robots How to Move"
date:   2016-10-07
---

In [the recent article on Google's research blog](https://research.googleblog.com/2016/10/how-robots-can-acquire-new-skills-from.html) a video showed how one researcher moved robot's hand to teach it to open a door. This looks exactly like [what Rethink Robotics are offering](https://www.youtube.com/watch?v=NwZvmD2McVI). Google's work is cutting edge research in cloud robotics / reinforcement learning and Rethink Robotics are already selling their products to industries. RR's robots are probably much more limited in their learning capabilities.
