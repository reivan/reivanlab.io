---
layout: post
title:  "Software Architecture Books"
date:   2016-10-10
tags: development
---

I've started working on a project where I'm partialy responsible for its architecture, so I decided to search for books. The most popular book on software architecture seem to be "[Design Patterns](https://en.wikipedia.org/wiki/Design_Patterns)" by Gang of Four. [There are many other books](http://www.designsmells.com/articles/ten-must-to-read-books-for-software-architects/) and I still don't know which one I should read. I guess I'll try some of them and see which one I like more.
