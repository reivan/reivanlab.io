---
layout: post
title:  "Only 2 weightings"
date:   2016-10-06
---

Stumbled upon an old problem and felt like drawing the solution:

![Only 2 weightings]({{site.url}}assets/img/05-Oct-2016_weights.png)

The task was to find an odd item out of 8 equally-weighted with only 2 weightings. It's clear from the picture that this technique will work for up to 9 items (if we constrain ourselves with only 2 weightings).

Actually I can't imagine this problem arising in the real world. First of all we can always compare weights using 2 of our hands. Hands are durable and can do much more than 2 weightings. But of course things can go wrong:

- Items are to heavy.
- The difference in weights of normal and odd items are too subtle.
- Can't touch items for some reason. Too hot, maybe?

Ok, no hands, but why weights support only 2 weightings? Only plausible explanation I can come up with is that weighting takes too much time or effort. This is consistent with items being too heavy, so maybe they are?

Anyway, what is item? It's something big and heavy but mostly identical to one another and weighting it is difficult. I think it is some industrial product. A car for example. In that case weighting might be used for quality assurance.
