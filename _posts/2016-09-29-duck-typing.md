---
layout: post
title:  "Duck Typing"
date:   2016-09-29 10:30:00 +0300
---

I didn't know what I should write in this site's description so I googled "duck quote" (using duckduckgo.com of course) and it gave me this:

> When I see a bird that walks like a duck and swims like a duck and quacks like a duck, I call that bird a duck.

This is probably where the [duck typing](https://en.wikipedia.org/wiki/Duck_typing) got it's name.
