---
layout: post
title:  "Interface-defined and Structure-defined Data Structures"
date:   2016-10-05
---

There are structures that are defined by their interface and there are structures that are defined by theirs, well, structure.

Interface-defined:

- Queue
- Stack
- Dictionary
- Priority Queue

Structure-defined:

- Array
- Linked-list
- Binary Search Tree

Interface-defined structures are implemented using structure-defined ones.
