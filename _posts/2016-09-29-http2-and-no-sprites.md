---
layout: post
title:  "HTTP/2 and no Sprites"
date:   2016-09-29
tags: development
---

Looks like there's no need to concatenate all the project files in one big bundle. It's better to think about what files change often and what files rarely change. Browsers will cache file that are not changing and won't be downloading them.

More info: [HTTP/2 For Web Developers](https://blog.cloudflare.com/http-2-for-web-developers/).
