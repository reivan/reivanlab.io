---
layout: page
title: Reading Club
permalink: /reading-club/
---

Once a month we meet up and discuss a book we've read.

## **Books we'd like to read / discuss**


### Algorithms to Live By: The Computer Science of Human Decisions

![](/assets/img/book_covers/alg_mind.jpg)

A fascinating exploration of how insights from computer algorithms can be applied to our everyday lives, helping to solve common decision-making problems and illuminate the workings of the human mind. [Goodreads Link](https://www.goodreads.com/book/show/25666050-algorithms-to-live-by).


### How to Read a Book

![](/assets/img/book_covers/how_to_read.png)

How can you learn the most from a book — or any other piece of writing — when  you're reading for information, rather than for pleasure? [PDF Link](http://pne.people.si.umich.edu/PDF/howtoread.pdf).


### Следующий уровень

![](/assets/img/book_covers/next_level.png)

Как заводить дружбу в нужных кругах, как работать над своим кругом знакомств, как использовать связи и быть полезным другим. [Goodreads Link](https://www.goodreads.com/book/show/29351834).


### So Good They Can't Ignore You: Why Skills Trump Passion in the Quest for Work You Love

![](/assets/img/book_covers/so_good.jpg)

After making his case against passion, Newport sets out on a quest to discover the reality of how people end up loving what they do. Spending time with organic farmers, venture capitalists, screenwriters, freelance computer programmers, and others who admitted to deriving great satisfaction from their work, Newport uncovers the strategies they used and the pitfalls they avoided in developing their compelling careers. [Goodreads Link](https://www.goodreads.com/book/show/13525945-so-good-they-can-t-ignore-you).


### Vernor Vinge's Works

![](/assets/img/book_covers/fast_times.jpg)

**Coming Age on Technological Singularity**: Within thirty years, we will have the technological means to create superhuman intelligence. Shortly after, the human era will be ended. [Link to the essay](http://www-rohan.sdsu.edu/faculty/vinge/misc/singularity.html).

**Fast Times at Fairmont High**: In a near future where everyone lives in a wireless-direct-mind-link, networked, sensory-enhanced world, two eighth-graders struggle to pass an exam for which using outside information sources is forbidden. [Goodreads Link](https://www.goodreads.com/book/show/5393037-fast-times-at-fairmont-high).

**Cookie Monster**: (Better reading knowing nothing in advance). [Link](https://www.ida.liu.se/~tompe44/lsff-book/Vernor%20Vinge%20-%20The%20Cookie%20Monster.htm).
<!-- https://www.wikiwand.com/en/The_Cookie_Monster_(novella) -->

<!-- https://www.goodreads.com/author/show/44037.Vernor_Vinge -->


### The Toaster Project: Or a Heroic Attempt to Build a Simple Electric Appliance from Scratch

![](/assets/img/book_covers/toaster.jpg)

In The Toaster Project, Thwaites asks what lies behind the smooth buttons on a mobile phone or the cushioned soles of running sneakers. What is involved in extracting and processing materials? To answer these questions, Thwaites set out to construct, from scratch, one of the most commonplace appliances in our kitchens today: a toaster. [Goodreads Link](https://www.goodreads.com/book/show/11467066-the-toaster-project).

<!-- 
### Asteroid Mining 101: Wealth for the New Space Economy

![](/assets/img/book_covers/asteroid_mining.jpg)

The emerging asteroid mining industry has extremely ambitious intentions. It is within the realm of possibility that their work may usher in a change in global economics as profound as the Industrial Revolution. [Goodreads Link](https://www.goodreads.com/book/show/24208246-asteroid-mining-101).
-->


## **Books we've read / discussed**


### The Dictator's Handbook: Why Bad Behavior is Almost Always Good Politics

![](/assets/img/book_covers/dictators.jpg)

Leaders do whatever keeps them in power. They don’t care about the “national interest”—or even their subjects—unless they have to. [Goodreads Link](https://www.goodreads.com/book/show/11612989-the-dictator-s-handbook).






<!-- 
###

![](/assets/img/book_covers/)

sdf. [Goodreads Link]().
 -->
